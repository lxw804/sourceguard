cmake_minimum_required(VERSION 3.15)
project(sourceguard-hook)

set(CMAKE_CXX_STANDARD 14)

include_directories(/home/a/program/azul/include /Users/a/program/azul_jdk8.0.212-macosx_x64/include)

add_library(sourceguard SHARED
        sourceguard.cpp
        cpu_id.h
        cpu_id.cpp
        rsa/rsa.cpp
        rsa/rsa.h
        base64/base64.h
        base64/base64.cpp
        org_magicwall_universal_core_BytecodeDecrypt.h
        com_fenquen_sourceguard_dencrypt_Dencrypt.h)
add_executable(rsa_test rsa/rsa_test.cpp rsa/rsa.h rsa/rsa.cpp base64/base64.cpp base64/base64.h)


find_package(OpenSSL REQUIRED)
if (OPENSSL_FOUND)
    include_directories(${OPENSSL_INCLUDE_DIRS})
    message(STATUS "OpenSSL Found!")
endif ()

target_link_libraries(rsa_test OpenSSL::Crypto OpenSSL::SSL)

#[[
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lopenssl")]]

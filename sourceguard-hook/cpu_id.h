//
// Created by a on 2021/5/30.
//

#ifndef SOURCEGUARD_HOOK_CPU_ID_H
#define SOURCEGUARD_HOOK_CPU_ID_H


std::string get_mac_and_cpu_id();
#define INVALID_MAC_AND_CPU_ID "INVALID_MAC_AND_CPU_ID"

#endif //SOURCEGUARD_HOOK_CPU_ID_H

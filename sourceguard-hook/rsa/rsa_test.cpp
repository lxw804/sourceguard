//
// Created by a on 2021/5/30.
//
#include "rsa.h"
#include "../base64/base64.h"

int main(int argc, char **argv) {

    //key length : 2048
    char plainText[2048 / 8] = "E8F408C0C8DABFEBFBFF000806C1";


    unsigned char encrypted[4098] = {0};
    unsigned char decrypted[4098] = {0};

    int encryptedLen;
    if ((encryptedLen = private_encrypt(reinterpret_cast<unsigned char *>(plainText),
                                        strlen(plainText),
                                        encrypted)) == -1) {
        perror("private_encrypt");
        exit(0);
    }
    const char *base64EncodedStr = base64_encode(encrypted, encryptedLen);
    printf("base64EncodedStr:%s\n", base64EncodedStr);

    unsigned char *base64Decoded = base64_decode(base64EncodedStr);
    if (public_decrypt(base64Decoded,
                       strlen(reinterpret_cast<const char *>(base64Decoded)),
                       decrypted) == -1) {
        perror("public_decrypt");
        exit(0);
    }
    printf("%s\n", decrypted);

    return 0;
}

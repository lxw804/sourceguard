//
// Created by a on 2021/5/30.
//
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <cstdio>
#include <cstring>

#ifndef SOURCEGUARD_HOOK_RSA_H
#define SOURCEGUARD_HOOK_RSA_H

#endif //SOURCEGUARD_HOOK_RSA_H

int public_decrypt(unsigned char *enc_data,
                   int data_len,
                   unsigned char *decrypted);

int private_encrypt(unsigned char *data,
                    int data_len,
                    unsigned char *encrypted);

//
// Created by a on 2021/5/30.
//
#include "rsa.h"

int padding = RSA_PKCS1_PADDING;

unsigned char publicKey[] = "-----BEGIN PUBLIC KEY-----\n"
                            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxKqvnd7wxkrPTTwM04gh\n"
                            "/qY6kqKcqOhWn8WE2j2BpDHtl5UY0/Xm5OmQ4A0xoMh3zP43HqTbVBnCv02iYBS6\n"
                            "qlCilKv7XS2QsDKka1W40sh5XlzXDd8wSJAt8SzagoIz+bm92mFUUGlCWBnv+I8x\n"
                            "L9TvmYssftU2kBsUbwFUY7L2cHjxsqtL0Dub9wKS0izN8xptpGmVHAGDwuiiwozN\n"
                            "k5LClYOSNr3uFAesTQw+ap+qit+tYNLMFCdNIiw+RvmBD35F7E0ROan6d18ZcatH\n"
                            "Hg3ZCCXZwg2LsUNwIm6APpdTr53gvCLoxOrHp8jhtE0YqCy78bjQScx93cqNmgxQ\n"
                            "pwIDAQAB\n"
                            "-----END PUBLIC KEY-----";


unsigned char privateKey[] = "-----BEGIN RSA PRIVATE KEY-----\n"
                             "MIIEpQIBAAKCAQEAxKqvnd7wxkrPTTwM04gh/qY6kqKcqOhWn8WE2j2BpDHtl5UY\n"
                             "0/Xm5OmQ4A0xoMh3zP43HqTbVBnCv02iYBS6qlCilKv7XS2QsDKka1W40sh5XlzX\n"
                             "Dd8wSJAt8SzagoIz+bm92mFUUGlCWBnv+I8xL9TvmYssftU2kBsUbwFUY7L2cHjx\n"
                             "sqtL0Dub9wKS0izN8xptpGmVHAGDwuiiwozNk5LClYOSNr3uFAesTQw+ap+qit+t\n"
                             "YNLMFCdNIiw+RvmBD35F7E0ROan6d18ZcatHHg3ZCCXZwg2LsUNwIm6APpdTr53g\n"
                             "vCLoxOrHp8jhtE0YqCy78bjQScx93cqNmgxQpwIDAQABAoIBAQC2dfF3E7bpEdEg\n"
                             "w/zWaG+5tXmUP//+iYiMtRFb9UKJFrVrNC4TwQnWCYibBkKr/DGB47051hOTBE91\n"
                             "c06Zu8GvcB5WtvT/7t1VS7bVynMuREauMI5pVU+Tkwi8wDN+b9RO9W/Z4wD5lZ6y\n"
                             "5GWKgzdMPE+Lcd73UVNNAYLEtkQYNfgEvWBcFejG/kkF41jk2SxIqWw+m8V42kXQ\n"
                             "DHz7k6HuuzHWMPFs1jWp0L6rA2QEAGNWDaxBwc2f6roKp4/us651Gfs6lx0NMSZp\n"
                             "40u1nxeuQzMGgPWUK9FeBN8SPAO7UIqWliMTCXFqskEwr6lOgJNXlB1H8jKJt0cZ\n"
                             "3rpTn7ghAoGBAPhZit2TFgBpPyPi9khLLv+/t42XP1FtRT2EcMdkxLfPrvEUdXIn\n"
                             "XabWfbDYZK5kIBuawc9QybpHegFV4pQEnQKwmDJgyfN1EJpE0v1I8w33UPdKJNKC\n"
                             "VmDwk5ybOFM3OBzQdyGrB3wdHpBEIfhACIZvSWAQZSXRNlfs/D2VC/8RAoGBAMq5\n"
                             "k8AA44ooUc7UyVKdUHMCmBXp9VYMr4GC5bD8KJZsLhYEWtn68NQ8RRyuKbyzDWGq\n"
                             "sEAflGVSHxr6TdKItXyJMZmCvIaGC6qhqtFKoV9hKBALWP6A5eHm+GHf17nYgXc3\n"
                             "qoFn7h2DdbhVsPMJQkb3Yc7YzZlRja2FTOQJOEQ3AoGBAI4aznbSvvglQNfL+piL\n"
                             "TK1SijkenkB9ge0dZ1rwsg9ravLPfhR/qfrZKLqUV6fh6u1gtB1oYMWSOKGi/y8r\n"
                             "JJxxZ+ULtQL1rTZYw1Hi+BC5vz6A0mkR41M81Xn651PWeOkj78b4EiRWlTJ/ieB+\n"
                             "42HpM9cgCoCFIc9V709dhavBAoGBAJvKWZ0oEbTOn2/PXpe6hWSdDbtpfDyQqx9g\n"
                             "NJv6bEaNBC2vUxJJImI3nvUXcsOs5g+0WJYurp/eqEDXMo8kuDPoStGqAq9uu/B5\n"
                             "JQiJzdG6nEsYLWAINqGQGjk9CE+t7nBruwCPmhHcQM9UKxPafbCD1Apd2kKExdgf\n"
                             "pKNp4m+pAoGAcSRwyCDHB6B2uWZ4QmbH/df8gvVHfCLwTlU9EGiI6S48SGAzzyS/\n"
                             "xIHYOwI331/GmXCwEb2LyGOA/3JfSlv8Cse8Td6/xDI6d/eDAXz99i+dvQmOVFDh\n"
                             "2TXGth3xY0wXd+g2bt3osByosi0M8UbShBWUacKXb26YWyMXjSV8Ft4=\n"
                             "-----END RSA PRIVATE KEY-----";

static RSA *createRSA(unsigned char *key, int public1) {
    RSA *rsa = nullptr;

    BIO *keybio = BIO_new_mem_buf(key, -1);
    if (keybio == nullptr) {
        printf("Failed to create key BIO");
        return nullptr;
    }

    if (public1) {
        rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, nullptr, nullptr);
    } else {
        rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, nullptr, nullptr);
    }

    if (rsa == nullptr) {
        printf("Failed to create RSA");
    }

    return rsa;
}


int private_encrypt(unsigned char *data,
                    int data_len,
                    unsigned char *encrypted) {
    RSA *rsa = createRSA(privateKey, 0);
    return RSA_private_encrypt(data_len, data, encrypted, rsa, padding);
}

int public_decrypt(unsigned char *enc_data,
                   int data_len,
                   unsigned char *decrypted) {
    RSA *rsa = createRSA(publicKey, 1);
    return RSA_public_decrypt(data_len, enc_data, decrypted, rsa, padding);
}



//
// Created by a on 2021/5/30.
//

#ifndef SOURCEGUARD_HOOK_BASE64_H
#define SOURCEGUARD_HOOK_BASE64_H

const char *base64_encode(unsigned char *a,int a_len);

unsigned char *base64_decode(const char *encodedBase64Str);

#endif //SOURCEGUARD_HOOK_BASE64_H

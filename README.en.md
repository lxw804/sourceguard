# sourceguard

基于字节码加密和jvmti的java源码保护方案,该项目也是本人对java源码保护的探索实践

## 项目结构

#### sourceguard-repack

```text
java编写 实现对原始jar文件的解包,对目标class文件加密,还原成jar文件
```

#### sourceguard-dencrypt

```text
java编写 使用native函数实现字节码加密和解密逻辑
```

#### sourceguard-hook

```text
c++编写 字节码解密/加密native函数的底层实现,利用jvmti的类加载回调函数实现字节码的解密
```

## 实验性功能

#### 生成的加密jar绑定指定服务器

在对原始的jar文件加密的时候可以指定要不要绑定服务器,如果需要在运行加密后的jar时会要求提供相应的激活码

## 重要说明

以字节码加密来实现源码保护遇到最大障碍便是是spring体系中大量使用的asm技术.spring aop会利用asm直接读取加密过的字节码,绕过了jvmti机制,使得程序报错。以本人的某个基于spring-boot
2.1.4的项目为例,需要额外更改下边的函数

```text
org.springframework.asm.ClassReader.ClassReader(byte[],int,boolean)
org.springframework.cglib.core.AbstractClassGenerator.generate(ClassLoaderData)
```

更改的原理无非是在原始逻辑调用读取到字节码之前对其进行还原

## 使用

#### 编译sourceguard-hook项目生成动态库
将该动态库放置在java.library.path对应的目录,ubuntu20.04是在/usr/lib

#### 使用sourceguard-repack更改原始的jar

```shell
java -jar sourceguard-repack.jar -orgin_jar_path a.jar -package_or_class_names com.a
选项说明
-origin_jar_path  必要 要加密的jar的path
-encrypted_jar_path 生成的加密的jar的path,默认是原始的jar名字后边增加"_encrypted"
-package_or_class_names 要加密的class的名字/隶属的package名字
-bind_machine 生成的加密的jar是不是绑定机器 y 需要 n不需要 默认n
启动
java -agentlib:sourcegaurd -jar a_encrypted.jar
```

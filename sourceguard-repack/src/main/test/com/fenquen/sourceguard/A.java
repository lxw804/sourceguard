package com.fenquen.sourceguard;

import com.fenquen.sourceguard.repack.Repackager;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class A {

    @Test
    public void a() throws Exception {
        List<String> a = new ArrayList<>();
        a.add("org.magicwall");
        Repackager.encryptJar("/home/a/message_bus_receive.jar", a);
    }
}
